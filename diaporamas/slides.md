<br><br>

# Journée ISN 2019 @ LORIA<br><br>

---

<br>Apprendre l'Histoire de l'informatique (et d'Internet) en jouant
<br><br><br>
Yannick Parmentier, LORIA - ESPE - Université de Lorraine



# Contexte

<font color="lightnavy">Le numérique dans le master "Métiers de l'Enseignement, de l'Education et de la Formation" (MEEF) 1er degré :</font><br>

|        | | |
|:-------|:--:|:--|
|UE&nbsp;710| &#x2b95;|<b>acquérir une culture numérique</b> et comprendre les enjeux/cadre institutionnels|
|UE&nbsp;810| &#x2b95;|concevoir des scénarios pédagogiques utilisant des outils et ressources numériques|
|UE&nbsp;1010| &#x2b95;|analyser des scénarios pédagogiques utilisant des outils et ressources numériques|
||||
|C2i2e|&#x2b95;|attester de compétences dans la _maîtrise des usages pédagogiques des outils et ressources numériques_|
||||



# Problématique
<br>

- Comment développer une culture numérique ? 

  &#x2b95; sensibiliser à l'impact croissant du numérique dans notre vie

  &#x2b95; faire découvrir l'Histoire de l'informatique
<br><br>

- Présentation classique (frontale)<!-- .element: class="fragment" data-fragment-index="2" -->

  &#x2b95; efficacité ? <!-- .element: class="fragment" data-fragment-index="2" -->
  
  &#x2b95; comment éviter d'avoir des étudiants passifs ? <!-- .element: class="fragment" data-fragment-index="2" -->



# Proposition
<br>
- Utiliser une présentation _ludique_ **impliquant** les étudiants<br><br>

  <p>&#x2b95; ludification (_gamification_)</p><!-- .element: class="fragment" data-fragment-index="2" -->

> « s'inspirer des ingrédients qui ont fait le succès des jeux – récompenses, défis, progression personnelle – pour les appliquer à d'autres domaines »<!-- .element: class="fragment" data-fragment-index="2" -->

<p style="float:right;">S. Cochard, Regards sur le Numérique</p><!-- .element: class="fragment" data-fragment-index="2" -->



# Ludification et jeux sérieux

- Jeu :

> ensemble de règles amenant des prises de décisions dans le but d'atteindre un objectif<!-- .element: class="fragment" data-fragment-index="1" -->

- Jeu sérieux (C. Abt, 1970) :<!-- .element: class="fragment" data-fragment-index="2" -->

> jeu dont les objectifs incluent un ou plusieurs apprentissages<!-- .element: class="fragment" data-fragment-index="2" -->

- Principales caractéristiques des jeux sérieux :<!-- .element: class="fragment" data-fragment-index="3" -->

  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;scénarisation (storytelling), immersion, interactivité<!-- .element: class="fragment" data-fragment-index="3" -->



# <br><br><br><br><br>Quelques Exemples de jeux sérieux



# 2025 exmachina [[lien]](http://www.2025exmachina.net/)
<br>
<center>
[![](http://www.serious-game.fr/wp-content/uploads/2014/05/2025-exmachina.jpg)<!-- .element: class="plain" -->](http://www.2025exmachina.net/)
</center>



# d0x3d [[lien]](https://d0x3d.com/d0x3d/welcome.html)
<iframe allowfullscreen="" frameborder="0" width="100%" height="500px" data-src="https://d0x3d.com/d0x3d/welcome.html"></iframe>



# Entanglion [[lien]](https://entanglion.github.io/)
<iframe allowfullscreen="" frameborder="0" width="100%" height="500px" data-src="https://entanglion.github.io/"></iframe>



# Privacy Board Game [[lien]](https://github.com/itidigitalbr/privacy-board-game/tree/master/board)
[![](images/privacy.png)<!-- .element: class="plain" -->](test)



# Potato pirates [[lien]](https://www.potatopirates.game/)
<iframe allowfullscreen="" frameborder="0" width="100%" height="500px" data-src="https://www.potatopirates.game/"></iframe>



# Fold it [[lien]](https://fold.it/portal/)
<iframe allowfullscreen="" frameborder="0" width="100%" height="500px" data-src="https://fold.it/portal/"></iframe>



# Zombilingo [[lien]](https://www.zombilingo.org)
<br>
<center>
![](https://inria.fr/var/inria/storage/images/medias/nancy/actualites-images/zombielogo/1280786-1-fre-FR/zombielogo_vignette.png)<!-- .element: class="plain" -->
</center>



# Intérêts de la ludification
<br>
- **Motivation** liée à l'autodétermination (Deci et Ryan, 1985) 

- **Engagement** par immersion, renforcement de comportements (Wastiau et al. 2009)

- Développement de **compétences cognitives et sociales** telles que coopération, planification, raisonnement logique (Ke, 2009)

- Meilleure **rétention des connaissances** (Sitzmann, 2011)

Mais :<!-- .element: class="fragment" data-fragment-index="2" -->

- nombreux biais (Squire, 2005) <!-- .element: class="fragment" data-fragment-index="2" -->

- généralisation souvent limitée (Tricot, 2017) <!-- .element: class="fragment" data-fragment-index="2" -->



# Clés pour une ludification réussie (Gagné et al., 1992)

1. Capter l'attention

2. Rendre les objectifs explicites

3. Réinvestir les apprentissages

4. Stimuler la réflexion

5. Rendre les règles explicites (et simples)

6. Encourager la performance (accessibilité)

7. Fournir de nombreux retours

8. Permettre une auto-évaluation continue



# <br><br><br><br><br>Enseigner l'Histoire de l'informatique



# Enseigner l'Histoire de l'informatique
<br>
- objectif pédagogique : 

  - connaître les dates clés de l'avènement de l'informatique
  
      &#x2b95; saisir la rapidité de la (r)évolution numérique

<br>
  
- objectif transverse :

  - développer des compétences sociales (coopération, etc.)

      &#x2b95; impliquer les étudiants 



# Idée de départ 
<br>
## co-construction d'une frise chronologique
<br>
<p style="float:left;" width="40%;">
<br><br>
<p style="float:left;" width="40%;" class="fragment" data-fragment-index="2">&#x2b95; Analogie avec le jeu <br> de cartes **Timeline** <br><br> [[lien vers tutoriel vidéo]](https://www.youtube.com/embed/bIL4Du_hvB8)</p>

![Timeline](https://www.jeuxdenim.be/images/jeux/Timeline_large02.jpg)<!-- .element: class="fragment" data-fragment-index="2" style="width:50%;float:right;" -->



# En pratique
<br>
1. sélectionner les **découvertes et dates clés** utilisées dans le jeu 

  &#x2192; appréhender la rapidité du développement de l'informatique

  &#x2192; focus sur les découvertes (et non les découvreur.euses)
  
2. récupérer des **images** (avec les droits adéquats) 

  &#x2192; wikipedia

3. sélectionner / concevoir un outil pour **produire des cartes** 

  &#x2192; voir page suivante

4. **réalisation pratique** : imprimer, plastifier et découper les cartes

  &#x2192; imprimante, plastifieuse, massicot



# &Agrave; propos de la création de cartes

- Solution de base : papier / stylo / photocopieur

- Solution avancée : outil de P.A.O. 

  1. solution WYSIWYG
  
    &#x2192; prise en main rapide
	
	&#x2192; fonctionnalités limitées
  
  2. solution à base de langage de description 

    &#x2192; prise en main plus longue
	
	&#x2192; expressivité
	
- Si besoin d'icônes : site [game-icons.net](https://game-icons.net/) (merci à Vincent Thomas pour cette référence)



### Cardgame Toolkit ([http://cardgametoolkit.com/](http://cardgametoolkit.com/))
<center>
[![](images/cardgame.png)<!-- .element: style="width:80%;" -->](http://cardgametoolkit.com/)
</center>



### Card Pen ([http://cardpen.mcdemarco.net/](http://cardpen.mcdemarco.net/))
<center>
[![](images/cardpen.png)<!-- .element: style="width:80%;" -->](http://cardpen.mcdemarco.net/)
</center>



### Board Game Deck Generator ([https://realadamsinger.github.io/board-game-deck-generator/](https://realadamsinger.github.io/board-game-deck-generator/))

<center>
<iframe width="100%" height="515" data-src="https://realadamsinger.github.io/board-game-deck-generator/" ></iframe>
</center>



### HCCD ([https://github.com/vaemendis/hccd/](https://github.com/vaemendis/hccd/))

<center>
[![](images/hccd.png) <!-- .element: style="width:80%" -->](https://github.com/vaemendis/hccd/)
</center>



### PyCard ([https://github.com/ghostsquad/pycard](https://github.com/ghostsquad/pycard))

<center>
[![](images/pycard.png) <!-- .element: width="90%" -->](https://github.com/ghostsquad/pycard)
</center>



### Squib ([http://squib.rocks/](http://squib.rocks/))

<center>
[![](images/squib.png)<!-- .element: style="width:50%" -->](http://squib.rocks/)
</center>



# En conclusion
<br>
- La **ludification** peut être **utile** dans un contexte d'apprentissage

- La ludification peut être mise en oeuvre via des **outils numériques et / ou physiques**

- La création d'activités ludiques **ne coûte pas** forcément **cher** <br>(en matériel, voire en temps)

- De **nombreuses sources d'inspiration** existent

- Il faut trouver le **bon thème** et les **bons mécanismes** (ni trop simples, ni trop complexes)

- _Attention à ne pas simplement faire apprendre à jouer au jeu&nbsp;!_



# Au fait : et notre jeu sérieux ?
<br>

<p style="float:left; width=40%">[Histoire de l'informatique - partie 1](http://cardgametoolkit.com/product/bits_history)
<br><br>[Histoire de l'informatique - partie 2](http://cardgametoolkit.com/product/bits_history2)
<br><br>[Histoire de l'informatique - partie 3](http://cardgametoolkit.com/product/bits_history3)
</p>

![](images/deck1-1.png)<!-- .element: class="plain" style="float:right;width:30%;" -->



# A vous de jouer
<br>
[[Lien vers la fiche d'activités]](images/atelier_jeux_serieux.pdf)



# Références

Abt, Clark. (1970). Serious Games. Viking Press.

Deci, Edward L., & Ryan, Richard M. (1985). Intrinsic motivation and selfdetermination in human behavior. New York : Plenum Press.

Gagné, Robert M. & Briggs, Leslie J. & Wager, Walter W. (1992). Principles of Instructional Design. 

Ke, Fengfeng. (2009). A Qualitative Meta-Analysis of Computer Games as Learning Tools. In: Ferdig, Richard E. (Ed.), Handbook of Research on Effective Electronic Gaming in Education. Hershey, PA: IGI Global, 1 - 32.



# Références (suite)

Sitzman, Tracy. (2011). A Meta-Analytic Examination of the Effectiveness of Computer-Based Simulation Games. In: Personal Psychology,
Vol. 64, No. 2, Summer 2011, 289 – 558.

Squire, Kurt. (2005). Changing the game: what happens when video games enter the classroom. innovate: journal of online education, 1(6).

Tricot, André. (2017). L'innovation pédagogique. Paris : Retz, collection Mythes et réalités. 

Wastiau, Patricia & Kearney, Caroline & Van den Berghe, Wouter (2009). Quels usages pour les jeux électroniques en classe ? Bruxelles (Belgique) : European Schoolnet.
