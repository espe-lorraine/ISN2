#! /bin/bash
# Exemple de corrigé du TP de bash
# Contact: yannick.parmentier@loria.fr
# Date: 2019/07/16
###################################

# On vérifie la ligne de commande
# (au cas où l'utilisateur demande de l'aide via -h) :
if [ "$1" = "-h" ];
then
    echo "Usage:"
    echo "    $0"
    echo "    programme permettant de faire des recherches sur les oeuvres du projet Gutenberg (www.gutenberg.org)"
    exit 0
fi

# On stocke le chemin du répertoire courant :
DIR=$(pwd)
#echo $DIR

echo "****************************"
echo "Bienvenue dans le TP de bash"
echo "****************************"

# S'il n'existe pas déjà, on construit un répertoire
# local tmp/ où stocker l'index puis les oeuvres :
if [ ! -d "$DIR/tmp" ];
then
    mkdir $DIR/tmp
fi
# On s'y rend :
cd $DIR/tmp/

# Création de l'index + vérification de l'encodage :
echo "Téléchargement de l'index des livres ..."
# Si l'index n'est pas présent :
if [ ! -f "GUTINDEX.ALL" ];
then
    # On le télécharge et on l'extrait
    wget "http://www.gutenberg.org/dirs/GUTINDEX.zip" && echo "terminé."
    unzip GUTINDEX.zip
fi
# Si l'encodage n'est pas UTF-8 :
encodage=$(file GUTINDEX.ALL | grep "UTF-8")
if [ "$encodage" = "" ];
then
    # On s'arrête
    echo "Problème d'encodage de l'index. Fin."
    exit 1
else
    echo "(index encodé en UTF-8)"
fi

# Affichage du menu interactif (tant que l'on ne quitte pas) :
stop="n"
while [ "$stop" != "o" ];
do
    echo "1. Afficher l'aide"
    echo "2. Effectuer une recherche"
    echo "3. Afficher les statistiques"
    echo "4. Vider le fichier de log (et les oeuvres téléchargées)"
    echo "5. Quitter"

    read reponse

    case $reponse in
	1)
	    echo "Usage:"
	    echo "    $0"
	    echo "    programme permettant de faire des recherches sur les oeuvres du projet Gutenberg (www.gutenberg.org)"
	    echo "" # Pour passer une ligne	    
	    ;;
	2)
	    # On demande les mots clés pour la recherche
	    ############################################
	    read -p "Saisissez les mots clés pour la recherche ? " mots

	    # Lancement de la recherche des oeuvres
	    ########################################
	    # On utilise "bash" plutôt que "./" au cas où
	    # les fichiers ne seraient pas exécutables
	    bash $DIR/recherche.sh $mots | bash $DIR/getBook.sh
	    echo "" # Pour passer une ligne	    
	    ;;
	3)
	    # Calcul (et affichage) des statistiques
	    ########################################
	    echo "Statistiques :"
	    bash $DIR/stats.sh
	    echo "" # Pour passer une ligne
	    echo ""
	    ;;
	4)
	    # Suppression du fichier de log
	    ###############################
	    if [ -f ~/.gutenberg.log ];
	    then
		rm ~/.gutenberg.log
	    fi
	    if [ -d $DIR/tmp ];
	    then
	       rm $DIR/tmp/*-8.txt 2> /dev/null # cf message d'erreur si vide
	    fi
	    echo "Remise à zéro effectuée"
	    echo ""
	    ;;
	5)
	    # Quitter
	    ##########
	    echo "Au revoir"
	    stop="o"
	    echo "" # Pour passer une ligne	    
	    ;;
	*)
	    echo "$reponse : Commande inconnue"
	    echo "" # Pour passer une ligne
	    ;;
    esac
done

# Fin (attendue)
################
exit 0
