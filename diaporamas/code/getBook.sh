#! /bin/bash
# Programme servant à télécharger des oeuvres de Gutenberg
# et à mettre à jour le fichier de log
# Contact: yannick.parmentier@loria.fr
# Date: 2019/07/16
#########################################

# Préfixe des oeuvres à télécharger
URL="http://www.gutenberg.org/files/"

# Pour chaque référence lue sur l'entrée standard
while read livre;
do
    #echo "$livre"
    # Si le livre en question n'a pas déjà été téléchargé (et est donc non présent) : 
    if [ ! -f "$livre-8.txt" ];
    then
	echo "téléchargement de l'oeuvre $livre ..."
	wget "$URL/$livre/$livre-8.txt" 2> /dev/null # On redirige les messages d'erreurs, si par exemple livre non présent
	# Si le wget a retourné un code retour différent de 0 (not equal)
	if [ "$?" != "0" ];
	then
	    echo "téléchargement de $livre-8.txt impossible"
	    continue # Pour passer au livre suivant
	fi
	# On extrait les données demandées pour le fichiers de log
	cat "$livre-8.txt" | grep -E "^(Title:|Author:|Release |Language:)" >> ~/.gutenberg.log
	echo "**********" >> ~/.gutenberg.log
    else
	echo "livre $livre déjà téléchargé !"
    fi
    # On attend une seconde pour télécharger un autre livre
    # (pour ne pas être black-listé par le serveur gutenberg.org)
    sleep 1 
done
