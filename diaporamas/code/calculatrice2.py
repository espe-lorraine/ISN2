import sys
from lark import Lark, tree

grammar = """
    ?start: npexp
            | NAME "=" npexp -> assign

    ?npexp: pexp
            | npexp "+" pexp -> add
            | npexp "-" pexp -> sub

    ?pexp: atom
            | pexp "*" atom -> mult
            | pexp "/" atom -> div

    ?atom: NAME                 -> var
            | NUMBER           -> val
            | "-" NUMBER       -> neg
            | "(" npexp ")"

    %import common.INT -> NUMBER
    %import common.CNAME -> NAME
    %import common.WS
    %ignore WS
"""

parser = Lark(grammar, start='start', ambiguity='explicit', parser="cyk")

if __name__ == '__main__':
    sentence = 'a = (-2) * 3 + 5 - 1'
    ast = parser.parse(sentence)
    print(ast)
    print(ast.pretty())
    if len(sys.argv) > 1:
        tree.pydot__tree_to_png(ast, sys.argv[1])
