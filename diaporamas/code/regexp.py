import re, sys

regexps = [r'^a(s|t)', r'n.*t$', r'(gre|st)', r'm.*m', r'^[^m]*m[^m]*m[^m]*$', r'^\D*[a-zA-Z]{4}\D*', r'^[a-zA-Z]{2}$']

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        print('Argument manquant\n\tUsage: '+sys.argv[0]+' fichier')
        sys.exit(1)
    # Argument passé en paramètre
    fichier = sys.argv[1]
    fout    = fichier+'.out'
    aout    = open(fout, 'w')

    for exp in regexps:
        print('###############################', file=aout)
        print(exp, file=aout)
        print('###############################', file=aout)        
        with open(fichier, 'r', encoding='iso-8859-1') as f:
            reponse = []
            for ligne in f:
                ville   = ligne.split(";")[0]
                automate = re.compile(exp, re.IGNORECASE)
                resultat = automate.findall(ville)
                if len(resultat) > 0:
                    reponse.append(ville)
            print(str(len(reponse)) + '\n', file=aout)
            print(reponse, file=aout)
