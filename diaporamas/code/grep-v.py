#!/usr/bin/env python3
"""Usage: python3 grep-v.py <pattern> <file>"""
import re
import sys

if __name__=='__main__':
    pattern, filename = sys.argv[1:] # on lit pattern, filename de la ligne de  commande
    print(pattern)
    matched = re.compile(pattern, re.IGNORECASE).search # on stock la fonction de recherche (application de l'automate) dans la variable matched
    with open(filename, encoding="utf-8") as f:
        for line in f:
            if not matched(line): # le patron ne sélectionne pas la ligne
                print(line)
            else:
                print('***', line)
