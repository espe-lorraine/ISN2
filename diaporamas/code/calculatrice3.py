import sys
from lark import Lark, tree, Transformer

grammar = """
    ?start: npexp
            | NAME "=" npexp -> assign

    ?npexp: pexp
            | npexp "+" pexp -> add
            | npexp "-" pexp -> sub

    ?pexp: atom
            | pexp "*" atom -> mult
            | pexp "/" atom -> div

    ?atom: NAME                -> var
            | NUMBER           -> val
            | "-" NUMBER       -> neg
            | "(" npexp ")"

    %import common.INT -> NUMBER
    %import common.CNAME -> NAME
    %import common.WS
    %ignore WS
"""

parser = Lark(grammar, start='start', ambiguity='explicit', parser="cyk")

class TreeToMIPS(Transformer):
    def __init__(self):
        self.last  = []  # pile pour recuperer les derniers registres utilises
        self.cpt   = 0   # numero suffixe du registre actuel
        self.regid = 'a' # lettre prefixe du registre actuel
        self.vars  = {}  # table des symboles

    def getReg(self): # pour avoir les registres où stocker les valeurs
        reg = self.regid + str(self.cpt) # calcul du nom de registre
        if self.cpt == 9: # a0..9, puis on passe a la lettre suivante
            self.regid = chr( (ord(self.regid)+1) % 26 ) # a, b, ..., z, a, ... 
            self.cpt   = 0
        else:
            self.cpt  += 1 # on incremente le numero (suffixe) de registre
        # on empile le registre
        self.last.append('$' + reg)
        return '$' + reg

    def pop(self): # pour lire le sommet de la pile (s'il existe)
        if len(self.last) > 0:
            return self.last.pop()
        else:
            return None
        
    def assign(self, a):
        last = self.pop()   #on charge le dernier registre utilise
        reg = self.getReg() #on reserve un nouveau registre
        self.vars[a[0]] = reg #on stocke la variable dans la table des symboles
        # suite parcours sur a[1], puis generation instruction move
        return '{}\n{} {}, {}'.format(a[1], 'move', reg, last)
    
    def add(self, a):
        last2 = self.pop()        
        last1 = self.pop()
        reg = self.getReg()
        # suite parcours sur a[0] puis a[1] puis generation instruction add
        return '{}\n{}\n{} {}, {}, {}'.format(a[0], a[1], 'add', reg, last1, last2)

    def sub(self, a):
        last2 = self.pop()
        last1 = self.pop()
        reg = self.getReg()        
        # suite parcours sur a[0] puis a[1] puis generation instruction sub
        return '{}\n{}\n{} {}, {}, {}'.format(a[0], a[1], 'sub', reg, last1, last2)

    def mult(self, a):
        last2 = self.pop()
        last1 = self.pop()
        reg = self.getReg()
        # suite parcours sur a[0] puis a[1] puis generation instruction mul     
        return '{}\n{}\n{} {}, {}, {}'.format(a[0], a[1], 'mul', reg, last1, last2)  

    def div(self, a):
        last1 = self.pop()
        last2 = self.pop()        
        reg = self.getReg()
        # suite parcours sur a[0] puis a[1] puis generation instruction div    
        return '{}\n{}\n{} {}, {}, {}'.format(a[0], a[1], 'div', reg, last1, last2)  

    def neg(self, a):
        reg = self.getReg()
        # nombre negatif = valeur absolue multiplie par -1
        return '{} {}, {}, {}'.format('mul', reg, '-1', a[0])

    def val(self, s):
        val = int(s[0])
        reg = self.getReg()
        # valeur reel correspond au chargement d'une constante (generation de l'instruction li)
        return '{} {}, {}'.format('li', reg, val)

    def var(self, s):
        var = s[0]
        reg = self.getReg()
        if var in self.vars: #variable deja rencontree, on recharge le registre
            old = self.vars[var]
            return '{} {}, {}'.format('move', reg, old)
        else: #variable rencontree pour la premiere fois, on reserve un registre
            self.vars[var] = reg
            return '{} {}, {}'.format('load', reg, var)


if __name__ == '__main__':
    sentences = ['a = (-2) * 3 + 5 - 1', '(-2) * 3 + 5 - 1 / 4', '(x - 1) * 3 + x * x']
    i  = 0
    for sentence in sentences:
        print('*********************\n'+sentence+'\n*********************')
        ast = parser.parse(sentence)
        #print(ast)
        #print(ast.pretty())
        if len(sys.argv) > 1:
            tree.pydot__tree_to_png(ast, sys.argv[1] + str(i) + '.png')
        i += 1
        astsem = TreeToMIPS().transform(ast)
        print(astsem)
