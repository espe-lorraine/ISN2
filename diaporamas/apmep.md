 <span class="menu-title" style="display: none">Projet PIAF</span>
![ESPE](images/logo_ul_espe_fond_transparent300dpi.png)<!-- .element: class="plain" style="width:55%;float:left;"--> 
![LORIA](images/logo_loria.png)<!-- .element: class="plain" style="width:12%;float:right;"--> 
<br><br><br>
<br><br>A04: _Des machines intelligentes (ou du moins qui apprennent) ?_
<br><br>

---

<br>
Journée Régionale de l'APMEP Lorraine - 20 mars 2019 <br>
<br><br><br>
Yannick Parmentier, Université de Lorraine, ESPE, LORIA 

Notes: mise en contexte de l'IA (Histoire, de nos jours, demain ? + un peu de pratique)



## Introduction
<br>
- Qu'est ce que l'Intelligence ? 

<quote>L'intelligence est <b>l'ensemble des processus</b> retrouvés dans des systèmes, plus ou moins complexes, vivants ou non, <b>qui permettent</b> de comprendre, d'apprendre ou <b>de s'adapter à des situations nouvelles</b>.</quote><!-- .element: class="fragment" data-fragment-index="2" --><span class="fragment" data-fragment-index="2" style="font-size:12pt;float:right;">(Source: [wikipedia](https://fr.wikipedia.org/wiki/Intelligence))</span>
<br><br>

- Une ou plusieurs (types d') Intelligence(s) ?<!-- .element: class="fragment" data-fragment-index="3" -->

<p>Liens avec la <b>déduction</b> (capacité d'anticipation, etc.)</p><!-- .element: class="fragment" data-fragment-index="3" -->

- Quelques domaines d'observation de l'intelligence :<!-- .element: class="fragment" data-fragment-index="4" -->

<p>Mathématiques, langage, jeux, etc. <b>&#x2192; systèmes de règles</b></p><!-- .element: class="fragment" data-fragment-index="4" -->

Notes: étymologiquement: intellego <=> choisir entre, domaines d'application <=> tous les systèmes de règles



## L'intelligence illustrée (?)
<br>
<center>
![](https://imgs.xkcd.com/comics/the_difference.png)<!-- .element: class="plain" style="height:500px;" -->
</center>
<span style="font-size:12px;float:right;">(Source: [XKCD]( https://xkcd.com/242/))</span>



## <br><br><br><br><br><br><br>Un peu d'Histoire de l'intelligence "artificielle"

---



## Intelligence et raisonnement 
<br>
<p>
**Logique** :
<br>
du grec λογική / logikê, est un terme dérivé de λόγος / lógos — signifiant la parole, la "parole parlante".
</p>

<p class="fragment" data-fragment-index="2" style="float:left;width:80%;"><br><b>Travaux d'Aristote (-384, -322)</b> : 
<br><br>&#x2192; Syllogisme</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:20%;">
<br>
![](https://upload.wikimedia.org/wikipedia/commons/a/a4/Aristoteles_Louvre.jpg)<!-- .element: class="plain" style="width:80%;" -->
<br>
<span class="fragment" data-fragment-index="2" style="font-size:12px;float:right;">(Source: [wikipedia](https://fr.wikipedia.org/wiki/Aristote))</span>
</p>

Notes: étude du raisonnement, Organon, prémisses + conclusion



## Peut on **automatiser** le raisonnement humain ?

<p class="fragment" data-fragment-index="2" style="float:left;width:80%;"><b>Travaux de Descartes (1596-1650) , Leibniz (1646-1716)</b>
<br><br>
&#x2192; La pensée rationnelle peut-elle être aussi **systématique** que l'algèbre ou la géométrie ?
<br><br>
« il n'y a[it] pas plus de besoin de se disputer entre deux philosophes qu'entre deux comptables. Car il leur suffirait de prendre leur crayon et leur ardoise en main, et de se dire l'un l'autre (avec un ami en témoin, au besoin) : Calculons ! »</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:20%;">
![](https://upload.wikimedia.org/wikipedia/commons/7/73/Frans_Hals_-_Portret_van_Ren%C3%A9_Descartes.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
![](https://upload.wikimedia.org/wikipedia/commons/6/6a/Gottfried_Wilhelm_von_Leibniz.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>

<p><br>&#x2192; Développement des **notations mathématiques** (langage formel)</p><!-- .element: class="fragment" data-fragment-index="3" -->

Notes: même époque -> travaux de Pascal (pascaline)



## Parenthèse : parmi l'héritage de Leibniz
<br>
<p>« Explication de l'arithmétique binaire » (1703)</p>

<p>
<center>
![](https://upload.wikimedia.org/wikipedia/commons/a/ac/Leibniz_binary_system_1703.png)<!-- .element: class="plain" style="width="30%;" -->
</center>
<br>
<span style="font-size:12px;float:right">(Source: [wikipedia](https://fr.wikipedia.org/wiki/Syst%C3%A8me_binaire))</span>
</p>

Notes: publication disponible à la BNF



## Peut on **modéliser** le raisonnement humain ?

<p class="fragment" data-fragment-index="2" style="float:left;width:80%;"><b>Travaux de Boole (1815-1864)</b>
<br><br>
&#x2192; Modélisation des propositions logiques 
<br><br>
« _Les lois de la pensée_ » (1854)
<br><br>Définition d'un *système formel* composé de :<br>

  - ensemble à deux valeurs : Vrai / Faux<br>
  
  - opérations : conjonction / disjonction / négation<br>

<br>
<span class="fragment" data-fragment-index="3">&#x2192; **Algèbre de Boole** <br>(analyse mathématique de la logique)</span></p>

<p class="fragment" data-fragment-index="2" style="float:right;width:20%;">
![](https://upload.wikimedia.org/wikipedia/commons/c/ce/George_Boole_color.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>



## Peut on **formaliser** le raisonnement humain ?

<p class="fragment" data-fragment-index="2" style="float:left;width:80%;"><b>Travaux de Frege (1848-1925), Peano (1858-1932), Hilbert (1862-1943), Russel (1872-1970)</b>
<br><br>
&#x2192; Étude du **langage** mathématique
<br><br>
Système formel composé de :<br>

  - ensemble de formules (faits)<br>

  - ensemble de déductions (règles d'inférence)<br>
  
  - un procédé d'interprétation (fonction mathématique)<br>

<br>
<span class="fragment" data-fragment-index="3">&#x2192; **Syntaxe** et **sémantique** (formelles) du langage</span>
<br><br>
<span class="fragment" data-fragment-index="4">&#x2192; Axiomatisation de l'arithmétique (Peano, 1889)</span>
</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:20%;">
![](https://upload.wikimedia.org/wikipedia/commons/9/99/Young_frege.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
![](https://upload.wikimedia.org/wikipedia/commons/3/3a/Giuseppe_Peano.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>



## Peut on **formaliser** le raisonnement humain ? (suite)
<br>
- Caractéristiques d'une _théorie mathématique_ (Frege, 1879) :

  - **cohérence** : impossibilité de démontrer une proposition et son contraire ;

  - **complétude** : pour tout énoncé, ou bien il est démontrable, ou bien son opposé est démontrable à l'intérieur de la théorie ;

  - **décidabilité** : il existe une procédure de décision permettant de tester tout énoncé de la théorie.

<p class="fragment" data-fragment-index="2">&#x2022; &Eacute;tude des fondements des mathématiques (Russell, 1913)</p>



<section data-background-iframe="https://www.du9.org/chronique/logicomix/"></section>



## Peut-on **formaliser** le raisonnement humain ? (suite)

<p class="fragment" data-fragment-index="2" style="float:left;width:80%;"><b>Travaux de Church (1903-1995), Gödel (1906-1978), Turing (1912-1954)</b>
<br><br>
&#x2192; Théorèmes d'incomplétude (Gödel, 1931) <br>&nbsp;&nbsp;&nbsp;&nbsp;(énoncés ni démontrables, ni réfutables)
<br><br>
&#x2192; Lambda-calcul (Church, 1930) 
<br><br>
&#x2192; Fonctions calculables (Church-Turing, 1936) 
<br><br>
&#x2192; **Machine de Turing** (Turing, 1936)
</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:20%;">
![](https://upload.wikimedia.org/wikipedia/commons/c/c1/1925_kurt_g%C3%B6del.png?uselang=fr)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
![](https://upload.wikimedia.org/wikipedia/commons/a/a1/Alan_Turing_Aged_16.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>



## Peut-on **mécaniser** le raisonnement humain ?

<p class="fragment" data-fragment-index="2" style="float:left;width:80%;"><b>Travaux de Babbage (1791-1871), Von Neumann (1903-1957) et Turing (1912-1954)</b>
<br><br>
&#x2192; Modèle théorique de **machine analytique** (1834) <br>&nbsp;&nbsp;&nbsp;(automatisation et paramétrisation des calculs)
<br><br>
&#x2192; Projet **Colossus** pour la cryptanalyse (1943) <br>&nbsp;&nbsp;&nbsp;(calculateur électronique binaire)
<br><br>
&#x2192; **Architecture de Von Neumann** (1945) <br>&nbsp;&nbsp;&nbsp;(programmes stockés - calculateur EDVAC)
</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:20%;">
![](https://upload.wikimedia.org/wikipedia/commons/6/6b/Charles_Babbage_-_1860.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
![](https://upload.wikimedia.org/wikipedia/commons/d/d6/JohnvonNeumann-LosAlamos.jpg?uselang=fr)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>

Notes: Architecture toujours utilisée de nos jours



## Naissance de l'intelligence artificielle (IA)

<p class="fragment" data-fragment-index="2" style="float:left;width:80%;"><b>Travaux de Wiener (1894-1964), McCulloch (1898-1969) et Pitts (1923-1969) </b>
<br><br>
&#x2192; Définition de la **cybernétique** (1947) <br>&nbsp;&nbsp;&nbsp;(étude des mécanismes de contrôle)
<br><br>
&#x2192; Définition du **neurone formel** (1943) <br>&nbsp;&nbsp;&nbsp;(modélisation mathématique de systèmes <br>&nbsp;&nbsp;&nbsp;&nbsp;biologiques)
<br><br>
&#x2192; **Test de Turing** (1950) <br>&nbsp;&nbsp;&nbsp;(reconnaître l'Homme de la machine)
<br><br>
&#x2192; Conférence de Dartmouth (1956) &#x2192; terme « **IA** »
</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:20%;">
![](https://upload.wikimedia.org/wikipedia/commons/4/4d/Norbert_wiener.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
![](https://upload.wikimedia.org/wikipedia/commons/c/c0/Lettvin_Pitts.jpg?uselang=fr)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>



## Premières applications

<p class="fragment" data-fragment-index="2" style="float:left;width:80%;"><b>Jouer, communiquer</b>
<br><br>
&#x2192; Premier programme du **jeu de dames** (1951) <br>&nbsp;&nbsp;&nbsp;(Strachey, Manchester)
<br><br>
&#x2192; Programme du **jeu d'échec** (1952) <br>&nbsp;&nbsp;&nbsp;(Turing, Manchester)
<br><br>
&#x2192; Programme **Eliza** (1966) <br>&nbsp;&nbsp;&nbsp;(Weizenbaum, MIT)
</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:20%;">
<br>
![](https://upload.wikimedia.org/wikipedia/commons/a/a4/Strachey_draughts_ferranti.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
![](https://www.masswerk.at/elizabot/weizenbaum-eliza.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>



<section data-background-image="images/eliza.png"><span style="float:right;"><font color="FFFFFF"><br>www.masswerk.at/eliza/[&nbsp;](https://www.masswerk.at/eliza/)</font></span></section>



## Balbutiements de l'Intelligence Artificielle

<p class="fragment" data-fragment-index="2" style="float:left;width:80%;"><b>Des moyens limités (jusqu'aux années 2000)</b>
<br>
(capacités de calcul / mémoire)
<br><br>
&#x2192; **Algorithmes génétiques** (1960+) <br>&nbsp;&nbsp;&nbsp;(problème d'optimisation, sélection "naturelle")
<br><br>
&#x2192; **Traitement automatique des langues** (1960+) <br>&nbsp;&nbsp;&nbsp;(grammaires formelles, théorie des langages)
<br><br>
&#x2192; **Systèmes experts** (1960+) <br>&nbsp;&nbsp;&nbsp;(systèmes de déduction + base de connaissances)
<br><br>
&#x2192; **Systèmes multi-agents** (1980+) <br>&nbsp;&nbsp;&nbsp;(interaction avec l'environnement)
</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:20%;">
<br>
![](https://social.technet.microsoft.com/wiki/cfs-filesystemfile.ashx/__key/communityserver-components-imagefileviewer/communityserver-wikis-components-files-00-00-00-00-05/8741.03_5F00_flood.png_2D00_550x0.png)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
![](https://upload.wikimedia.org/wikipedia/commons/b/b5/Honda_E0_Fan_Fun_Lab.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>



## Renouveau de l'Intelligence Artificielle

<p class="fragment" data-fragment-index="2" style="float:left;width:60%;"><b>Des moyens "illimités" (2000+)</b>
<br><br>
&#x2192; **Reconnaissance vocale / de formes** <br>&nbsp;&nbsp;&nbsp;&nbsp;(cf téléphones portables)
<br><br>
&#x2192; **Véhicules autonomes**
<br><br>
&#x2192; **Recommandation** <br>&nbsp;&nbsp;&nbsp;&nbsp;(cf sites de vente en ligne)
<br><br>
&#x2192; **Traduction automatique** <br>&nbsp;&nbsp;&nbsp;&nbsp;(cf moteurs de recherches)
</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:40%;">
<br>
![](https://s3-ap-south-1.amazonaws.com/av-blog-media/wp-content/uploads/2018/03/MNIST.png)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
![](https://upload.wikimedia.org/wikipedia/commons/e/ec/Jurvetson_Google_driverless_car_trimmed.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>



## L'Intelligence Artificielle de nos jours

<p class="fragment" data-fragment-index="2" style="float:right;width:40%;">
<br>
![](https://upload.wikimedia.org/wikipedia/commons/7/71/Carto_IA_deepLearning.svg)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br><br><br>
<span style="font-size:12px;float:right;">(Source: [wikipedia](https://fr.wikipedia.org/wiki/Intelligence_artificielle))</span>
<br>
![](https://makina-corpus.com/blog/metier/2017/imlp/reve_ia)<!-- .element: class="fragment" data-fragment-index="4" style="width:80%;float:right;" -->
</p>

<p class="fragment" data-fragment-index="2" style="float:left;width:60%;"><b>Apprentissage automatique</b>
<br><br>
Ensemble de **techniques** permettant à une machine d’**apprendre à réaliser une tâche** sans avoir à la programmer **explicitement** pour cela
</p>

<p class="fragment" data-fragment-index="3" style="float:left;width:60%;">
&#x2192; Différents **types** d'apprentissage : <br>supervisé, non-supervisé, par transfert, par renforcement
</p>

<p class="fragment" data-fragment-index="4" style="float:left;width:60%;">
	&#x2192; Différents **algorithmes** : <br> régression (linéaire), arbre de décision, _apprentissage profond_, etc.
</p>



## <br><br><br><br><br><br><br>Apprendre à une machine (en pratique)

---



## Choix du problème

<p class="fragment" data-fragment-index="2" style="float:right;width:40%;">
<br>
![](https://upload.wikimedia.org/wikipedia/commons/3/32/Tic_tac_toe.svg)<!-- .element: class="plain" style="width:40%;float:right;" -->
<br><br><br><br><br><br>
![](https://i1.wp.com/chalkdustmagazine.com/wp-content/uploads/2016/03/menace.jpg?resize=300%2C186)<!-- .element: class="fragment" data-fragment-index="3" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
<span class="fragment" data-fragment-index="3" style="font-size:12px;float:right;">(Source: [chalkdust](http://chalkdustmagazine.com/features/menace-machine-educable-noughts-crosses-engine/))</span>
</p>

<p class="fragment" data-fragment-index="2" style="float:left;width:60%;"><b>Le jeu du morpion</b>
<br><br>
Apprendre à ne pas perdre au jeu du morpion (ne pas laisser l'adversaire aligner horizontalement ou vertical- ement trois symboles identiques)
</p>

<p class="fragment" data-fragment-index="3" style="float:left;width:60%;"><b>La machine M.E.N.A.C.E.</b> (Mitchie, 1961)
<br><br>
Apprentissage par renforcement en jouant plusieurs parties
</p>



## La machine M.E.N.A.C.E.

<p class="fragment" data-fragment-index="2" style="float:right;width:40%;">
<br>
![](http://stmlcom.s3.amazonaws.com/jamesbridle-playful.024.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
<span style="font-size:12px;float:right;">(Source: [jamesbridle.com](http://jamesbridle.com/))</span>
<br><br><br>
![]()<!-- .element: class="plain" -->
</p>

<p class="fragment" data-fragment-index="2" style="float:left;width:60%;"><b>Structure</b>
<br><br>
&#x2022; 304 boîtes d'allumettes pour chaque **configuration** possible d'une grille de morpion ($3^9$ - |symétries+rotations|)
<br><br>
&#x2022; $n$ perles par coup jouable $c$ &nbsp;($c \leq 9$)
<br> en fonction du tour de jeu
</p>

<p class="fragment" data-fragment-index="3" style="float:left;width:60%;"><b>Déroulé d'une partie</b>
<br>
![](http://www.mscroggs.co.uk/img/full/menace-example.png)
<span style="font-size:12px;float:right;">(Source: [mscroggs.co.uk](http://www.mscroggs.co.uk))</span>
</p>



## La machine M.E.N.A.C.E. (suite)

<p style="float:right;width:30%;">
<br>
![](https://i2.wp.com/chalkdustmagazine.com/wp-content/uploads/2016/03/img4.jpg?resize=344%2C1024)<!-- .element: class="plain" style="width:50%;float:right;" -->
<br><br><br><br><br><br><br><br><br><br><br><br>
<span style="font-size:12px;float:right;">(Source: [chalkdust](http://chalkdustmagazine.com/features/menace-machine-educable-noughts-crosses-engine/))</span>
<br><br><br>
![]()<!-- .element: class="plain" -->
</p>

<p style="float:left;width:70%;"><b>Fin de la partie</b>
<br><br>
&#x2022; si M.E.N.A.C.E. a gagné : **récompense** <br>(+3 perles dans chaque configuration visitée)
<br><br>
&#x2022; si elle a fait match nul : **récompense** <br>(+1 perle)
<br><br>
&#x2022; si elle a perdu : **pénalité** (-1 perle)
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![](https://i1.wp.com/chalkdustmagazine.com/wp-content/uploads/2016/03/img5.jpg?resize=768%2C188)<!-- .element: class="plain" style="width:60%;" -->
</p>



## La machine M.E.N.A.C.E. (suite)

<p style="float:left;width:100%;"><b>Capacités d'apprentissage</b>
<br><br>
![](https://i0.wp.com/chalkdustmagazine.com/wp-content/uploads/2016/03/original.jpg?w=721)<!-- .element: class="plain" style="width:40%;float:left;" -->
![](https://i0.wp.com/chalkdustmagazine.com/wp-content/uploads/2016/03/other.jpg?w=721)<!-- .element: class="plain" style="width:40%;float:right;" -->
<br><br><br><br><br><br><br><br><br><br><br>
<span style="font-size:12px;float:right;">(Source: [chalkdust](http://chalkdustmagazine.com/features/menace-machine-educable-noughts-crosses-engine/))</span>
</p>



<section data-background-image="images/menace.png"><span style="float:left;"><font color="000000">
<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
github.com/mscroggs/MENACE[&nbsp;](https://github.com/mscroggs/MENACE)</font></span></section>



## Un jeu de morpion "intelligent"
<br>
<p style="float:left;width:60%"><img class="plain" style="width:100%" data-src="images/minmax.png"/></p>

<p style="font-size:18px;float:right;width:40%;">Parcours de l'arbre pour faire remonter à la racine une valeur calculée récursivement :
<br><br>
&#x2022; $minimax(p) = f(p)$ 
<br><br>
si $p$ est une feuille de l'arbre, et où $f$ est une fonction d'évaluation du jeu ;
<br><br>

&#x2022; $minimax(p) = MAX(minimax(q1),$ $\dots, minimax(qn))$
<br><br>
si $p$ est un noeud *Joueur* avec fils $q1,\dots,qn$ ;
<br><br>
&#x2022; $minimax(p) = MIN(minimax(q1),$ $\dots, minimax(qn))$
<br><br>
si $p$ est un noeud *Opposant* avec fils $q1,\dots, qn$.
</p>

<p class="fragment" data-fragment-index="2" style="font-size:18px;">&nbsp;<br><br>NB: $f$ peut aussi être apprise en jouant plusieurs parties<br>(cf. M.E.N.A.C.E.)</p>



## Programmation du jeu de morpion
<br>
A vous de jouer : voir énoncé de Vincent Thomas
<center>
[![](images/TP-0.png)<!-- .element: style="width:40%" -->](https://members.loria.fr/vthomas/mediation/ISN_2014/TP.pdf)
</center>



## Application à d'autres jeux ?
<br>
<p class="fragment" data-fragment-index="2" style="float:right;width:35%;">
![](https://upload.wikimedia.org/wikipedia/commons/4/4a/Go_game.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br>
<span class="fragment" data-fragment-index="3" style="font-size:12px;float:right;">(Source: [wikipedia](https://fr.wikipedia.org/wiki/Go_(jeu))</span>
<br>
![](http://stmlcom.s3.amazonaws.com/jamesbridle-playful.026.jpg)<!-- .element: class="fragment" data-fragment-index="3" style="width:80%;float:right;" -->
<br><br><br><br><br>
<span class="fragment" data-fragment-index="3" style="font-size:12px;float:right;">(Source: [jamesbridle.com](http://shorttermmemoryloss.com/menace/))</span>
</p>

<p class="fragment" data-fragment-index="2" style="float:left;width:65%;"><b>Jeux déterminés à information complète</b>
<br><br>
échecs, dames, go, etc.
</p>

<p class="fragment" data-fragment-index="3" style="float:left;width:65%;">&nbsp;<br><b>Problématique</b>
<br><br>
Nombre de coups très grand 
<br>
&#x2192; utilisation d'**heuristiques**
</p>



## Conclusion
<br>
- Intelligence artificielle :

  - méthodes symboliques (choix explicites)
  
  - méthodes statistiques (choix implicites)<br><br>
  
- Dépendance aux données d'apprentissage<br><br>

- Applications multiples : transport, loisir, art, etc.<br><br>

- Soulève de nouvelles questions : droit, éthique, etc.



## Au sujet des femmes en informatique

<p class="fragment" data-fragment-index="2" style="float:left;width:70%;"><b>Une Histoire riche, e.g.,</b>
<br><br>
&#x2192; Ada Lovelace (1843) <br>&nbsp;&nbsp;&nbsp;(Premier programme)
<br><br>
&#x2192; Harvard Computers (1880) <br>&nbsp;&nbsp;&nbsp;(Travaux en astronomie)
<br><br>
&#x2192; Women's Royal Navy Service (WRNS) <br>&nbsp;&nbsp;&nbsp;(2nde Guerre Mondiale)
<br><br>
&#x2192; Margaret Hamilton (1969) <br>&nbsp;&nbsp;&nbsp;(programme Apollo de la NASA)
</p>

<p class="fragment" data-fragment-index="2" style="float:right;width:30%;">
![](https://upload.wikimedia.org/wikipedia/commons/4/4b/Colossus.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
<br><br><br><br><br><br>
![](https://upload.wikimedia.org/wikipedia/commons/d/db/Margaret_Hamilton_-_restoration.jpg)<!-- .element: class="plain" style="width:80%;float:right;" -->
</p>



## Quelques références
<br>
- [Initiation à l'apprentissage automatique avec Python (théorie)](https://makina-corpus.com/blog/metier/2017/initiation-au-machine-learning-avec-python-theorie)

- [Initiation à l'apprentissage automatique avec Python (pratique)](https://makina-corpus.com/blog/metier/2017/initiation-au-machine-learning-avec-python-pratique)

- Page wikipedia [« Les femmes en informatique »](https://en.wikipedia.org/wiki/Women_in_computing) (en anglais)

- Page wikipedia sur l'[Apprentissage automatique](https://fr.wikipedia.org/wiki/Apprentissage_automatique)

- [Tutoriels de Vincent Thomas (LORIA)](https://members.loria.fr/vincent.thomas/tutoriels.html)

- Page wikipedia sur l'[Histoire de l'Intelligence Artificielle](https://fr.wikipedia.org/wiki/Histoire_de_l%27intelligence_artificielle)

- [Application Javascript de la machine M.E.N.A.C.E.](http://www.mscroggs.co.uk/menace/)



## <br><br><br><br><br><br><br>Merci pour votre attention
